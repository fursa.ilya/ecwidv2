package com.fursa.ecwid.ext

import android.content.Context
import android.widget.Toast
import com.fursa.ecwid.data.ProductEntity
import com.fursa.ecwid.ui.model.ProductModel

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Context.toast(messageId: Int) {
    Toast.makeText(this, messageId, Toast.LENGTH_LONG).show()
}

fun ProductEntity.toModelView(): ProductModel {
    return ProductModel(
        productTitle.toString(),
        productPrice!!.toDouble(),
        productId!!
    )
}