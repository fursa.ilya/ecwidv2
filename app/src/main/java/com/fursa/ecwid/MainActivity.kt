package com.fursa.ecwid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.fursa.ecwid.data.ProductViewModel
import com.fursa.ecwid.ext.toast
import com.fursa.ecwid.ui.CreateDialog
import com.fursa.ecwid.ui.DetailsFragment
import com.fursa.ecwid.ui.adapter.ProductListAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), CreateDialog.OnUpdateProductsListener, ProductListAdapter.OnDetailsClickListener {
    private lateinit var viewModel: ProductViewModel
    private val subs = CompositeDisposable()
    private val productAdapter = ProductListAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

        fab.setOnClickListener { openCreateDialog() }

        getProducts()

        with(productList) {
            adapter = productAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        }
    }

    private fun openCreateDialog() {
        val dialog = CreateDialog.getInstance()
        dialog.show(supportFragmentManager, dialog.tag)
    }

    private fun getProducts() {
        subs.add(
            viewModel.selectAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { products ->
                   if(products.isEmpty()) {
                       txtMessage.visibility = View.VISIBLE
                       productList.visibility = View.GONE
                   } else {
                       txtMessage.visibility = View.GONE
                       productList.visibility = View.VISIBLE
                       productAdapter.setProducts(products)
                   }
                }
        )
    }

    override fun onProductsUpdated() {
        getProducts()
        this.toast("Update listener")
    }

    override fun onDetailsClicked(productId: Long) {
        val detailsDialog = DetailsFragment.getInstance(productId)
        detailsDialog.show(supportFragmentManager, detailsDialog.tag)
    }

    override fun onDestroy() {
        super.onDestroy()
        subs.clear()
    }

}
