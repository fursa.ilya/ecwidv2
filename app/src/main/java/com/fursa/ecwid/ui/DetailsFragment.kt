package com.fursa.ecwid.ui

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.fursa.ecwid.R
import com.fursa.ecwid.data.ProductEntity
import com.fursa.ecwid.data.ProductViewModel
import com.fursa.ecwid.ext.toast
import com.fursa.ecwid.ui.model.ProductModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_detals.*

const val PRODUCT_ID = "id"

class DetailsFragment: DialogFragment() {
    private val subs = CompositeDisposable()
    private lateinit var viewModel: ProductViewModel
    private var listener: CreateDialog.OnUpdateProductsListener? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is CreateDialog.OnUpdateProductsListener) {
            listener = context
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if(dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detals, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { dismiss() }
        viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        context?.toast("Полученный id: ${arguments?.getLong(PRODUCT_ID)}")
        val productId = arguments?.getLong(PRODUCT_ID)
        if (productId != null) {
            getDetails(productId)
        }

        buttonUpdate.setOnClickListener {
           val newProduct = ProductModel(
               editTextProductTitle.text.toString(),
               editTextProductPrice.text.toString().toDouble(),
               productId!!)
           viewModel.updateProduct(newProduct)
           listener?.onProductsUpdated()
           dismiss()
        }

        imageButton.setOnClickListener {
            context?.toast("Image")
        }

    }

    private fun getDetails(productId: Long) {
       subs.addAll(
           viewModel.getDetails(productId)
               .subscribeOn(Schedulers.io())
               .observeOn(AndroidSchedulers.mainThread())
               .subscribe(
                   { result -> bindDetails(result)},
                   { error -> showError(R.string.error_message)})
       )
    }

    private fun bindDetails(result: ProductEntity?) {
        val title = result?.productTitle
        val price = result?.productPrice
        context?.toast("Title: ${title} Price: ${price} руб.")
        editTextProductTitle.text = Editable.Factory.getInstance().newEditable(title)
        editTextProductPrice.text = Editable.Factory.getInstance().newEditable(price.toString())
    }

    private fun showError(message: Int) {
        context?.toast(message)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onDestroy() {
        super.onDestroy()
        subs.clear()
    }

    companion object {
        fun getInstance(productId: Long): DetailsFragment {
            val fragment = DetailsFragment()
            val args = Bundle()
            args.apply {
                putLong(PRODUCT_ID, productId)
            }
            fragment.arguments = args
            return fragment

        }
    }
}