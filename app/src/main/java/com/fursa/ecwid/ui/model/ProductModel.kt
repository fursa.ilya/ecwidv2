package com.fursa.ecwid.ui.model

data class ProductModel(
    var title: String,
    var price: Double,
    var productId: Long
)