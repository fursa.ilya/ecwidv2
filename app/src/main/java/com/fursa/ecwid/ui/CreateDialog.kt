package com.fursa.ecwid.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.fursa.ecwid.R
import com.fursa.ecwid.data.ProductEntity
import com.fursa.ecwid.data.ProductViewModel
import com.fursa.ecwid.ext.toast
import kotlinx.android.synthetic.main.dialog_create.*
import kotlinx.android.synthetic.main.fragment_detals.*

class CreateDialog : RoundedBottomSheetDialog() {
    private lateinit var viewModel: ProductViewModel
    private var listener: OnUpdateProductsListener? = null

    companion object {
        fun getInstance(): CreateDialog {
            return CreateDialog()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is OnUpdateProductsListener) {
            listener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_create, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        buttonDown.setOnClickListener { dismiss() }
        buttonCreate.setOnClickListener {
            val product = ProductEntity(
                productTitle = editTextProductTitle?.text?.toString(),
                productPrice = editTextProductPrice?.text?.toString()?.toDouble()
            )

            viewModel.save(product)
            context?.toast("Новый продукт: ${product.productTitle} создан")
            listener?.onProductsUpdated()
            dismiss()
        }

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnUpdateProductsListener {
        fun onProductsUpdated()
    }
}