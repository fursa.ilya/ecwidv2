package com.fursa.ecwid.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fursa.ecwid.R
import com.fursa.ecwid.data.ProductEntity
import com.fursa.ecwid.ext.toModelView
import com.fursa.ecwid.ui.model.ProductModel

class ProductListAdapter(private val listener: OnDetailsClickListener) : RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>() {

    private var allProducts = mutableListOf<ProductEntity>()

    fun setProducts(productEntities: List<ProductEntity>) {
        allProducts.clear()
        allProducts.addAll(productEntities)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val productView = LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false)
        return ProductViewHolder(productView)
    }

    override fun getItemCount() = allProducts.count()

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = allProducts[position]
        holder.bind(product.toModelView())
    }

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val txtTitle = itemView.findViewById<TextView>(R.id.txtProductTitle)
        private val txtPrice = itemView.findViewById<TextView>(R.id.txtProductPrice)
        private var productId: Long = 0

        fun bind(product: ProductModel) {
            txtTitle.text = product.title
            txtPrice.text = "${product.price} руб."
            productId = product.productId

            itemView.setOnClickListener {
               listener.onDetailsClicked(productId)
            }
        }
    }

    interface OnDetailsClickListener {
        fun onDetailsClicked(productId: Long)
    }

}