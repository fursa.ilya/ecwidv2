package com.fursa.ecwid.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(productEntity: ProductEntity)

    @Query("SELECT * FROM Product")
    fun selectAll(): Flowable<List<ProductEntity>>

    @Query("SELECT * FROM Product WHERE id=:id")
    fun getDetails(id: Long): Single<ProductEntity>

    @Query("UPDATE Product SET title=:title, price=:price WHERE id=:id")
    fun updateProduct(title: String, price: Double, id: Long?)
}