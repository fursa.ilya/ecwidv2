package com.fursa.ecwid.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.fursa.ecwid.ui.model.ProductModel
import io.reactivex.Flowable
import io.reactivex.Single

class ProductViewModel(application: Application) : AndroidViewModel(application) {

    private val localStorage by lazy {
        LocalStorage.getLocalStorage(application.applicationContext)
    }

    private var repository: ProductRepository = ProductRepository(localStorage.getProductDao())

    fun selectAll(): Flowable<List<ProductEntity>> = repository.selectAll()

    fun save(productEntity: ProductEntity) = repository.save(productEntity)

    fun getDetails(id: Long): Single<ProductEntity> = repository.getDetails(id)

    fun updateProduct(product: ProductModel) {
        repository.updateProduct(product)
    }
}