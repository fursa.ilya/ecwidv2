package com.fursa.ecwid.data

import com.fursa.ecwid.ui.model.ProductModel
import io.reactivex.Flowable
import io.reactivex.Single

class ProductRepository(private val productDao: ProductDao) {

    fun save(productEntity: ProductEntity) { productDao.save(productEntity) }

    fun selectAll(): Flowable<List<ProductEntity>> = productDao.selectAll()

    fun getDetails(id: Long): Single<ProductEntity> = productDao.getDetails(id)

    fun updateProduct(productModel: ProductModel) {
       productDao.updateProduct(
           productModel.title,
           productModel.price,
           productModel.productId)
    }

}