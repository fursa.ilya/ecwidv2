package com.fursa.ecwid.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Product")
data class ProductEntity(
    @ColumnInfo(name = "title")
    var productTitle: String?,
    @ColumnInfo(name = "price")
    var productPrice: Double?
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var productId: Long? = 0
}