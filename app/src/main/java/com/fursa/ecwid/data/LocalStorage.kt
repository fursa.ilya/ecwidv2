package com.fursa.ecwid.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

const val localDatabaseName = "ProductEntity.db"

@Database(entities = [ProductEntity::class], version = 3, exportSchema = false)
abstract class LocalStorage: RoomDatabase() {

    companion object {

        @Synchronized fun getLocalStorage(context: Context): LocalStorage {
            return Room.databaseBuilder(context.applicationContext, LocalStorage::class.java, localDatabaseName)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }
    }

    abstract fun getProductDao(): ProductDao
}